# Terraform CI

This repo stores templates used in other projects to avoid duplication and ease re-use.


* Terraform.gitlab-ci.yml : integration with gitlab project (environments, )
* Terraform-Base.gitlab-ci.yml : terraform commands hidden gitlab-jobs, extended in Terraform.gitlab-ci.yml

<!-- ## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->

## Installation / Usage

<!-- Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection. -->

<!-- Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. -->


To use the templates in this repo, insert the following lines in your project's `.gitlab-ci.yml` 

* Example: Always follow the latest version:
```
include:
- project: 'nomadic-labs/sandboxes/template/terraform-ci'
  ref: main
  file: '/gitlab-ci/Terraform.gitlab-ci.yml'
```

* Example: Pinpoint to a specific revision, for example `1.0.0` (tag must exist in the template repo)
```
include:
- project: 'nomadic-labs/sandboxes/template/terraform-ci'
  ref: 1.0.0
  file: '/gitlab-ci/Terraform.gitlab-ci.yml'
```

* Example: specific branch, e.g. `staging` (must exist in the template repo)
```
include:
- project: 'nomadic-labs/sandboxes/template/terraform-ci'
  ref: 'staging'
  file: '/gitlab-ci/Terraform.gitlab-ci.yml'
```

## Support
Nomadic-Labs Devops

## Roadmap
<!-- If you have ideas for releases in the future, it is a good idea to list them in the README. -->

- [ ] Add terraform lintint
- [ ] should CI of this current project  be different than the one it will include ? (revise includes if so...)
- [x] Add destroy but restricted (variable) (temporary solution). 
   - [ ] Module needs to be revised - rework the provider pattern for the DNS peering)
- [ ] Refine gitlab environment usage / link with terraform (dev/staging/prod ? )
- [ ] Integrate terraform workspace


<!-- ## Contributing -->
<!-- State if you are open to contributions and what your requirements are for accepting them.
For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.
You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser. -->


## Authors and acknowledgment

* Corentin Méhat for Nomadic Labs, 2024
* forked from deprecated https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml
* inspired by https://medium.com/google-cloud/running-terraform-cicd-pipelines-on-gcp-with-gitlab-5611d3a87a46

## License
* MIT Licence, Nomadic Labs
* logos from https://www.svgrepo.com/svg/448226/gitlab, https://www.svgrepo.com/svg/448253/terraform, https://www.svgrepo.com/svg/372275/ci-cd 


## Project status
<!-- If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
